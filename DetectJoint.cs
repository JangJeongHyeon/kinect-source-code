﻿using UnityEngine;
using System.Collections;
using Windows.Kinect;

public class DetectJoint : MonoBehaviour {

    public GameObject BodySrcManager;
    public JointType TrackedJoint;

    private HandState TrackedHandState;
    private BodySourceManager BodyManager;
    private Body[] bodies;

	// Use this for initialization
	void Start () {

        if (BodySrcManager == null)
        {
            Debug.Log("Body Soruce Manager C# script(GameObject) 가 에러잼잼");
        }
        else
        {
            BodyManager = BodySrcManager.GetComponent<BodySourceManager>();

        }
	}
	
	// Update is called once per frame
	void Update () {
	    if(BodyManager == null)
        {
            return;
        }

        bodies = BodyManager.GetData();

        if(bodies == null)
        {
            return;
        }

        foreach(var body in bodies)
        {
            if(body == null)
            {
                continue;
            }
            if (body.IsTracked)
            {
                var pos = body.Joints[TrackedJoint].Position;
                var State = body;
                if (State.HandLeftState == HandState.Closed)
                {
                    Debug.Log("닫힘\n");
                }
                else if(State.HandLeftState == HandState.Lasso)
                {
                    Debug.Log("왼쪽 두손가락ㅇ\n");
                }
                else if (State.HandLeftState == HandState.Open)
                {
                    Debug.Log("열림\n");
                    gameObject.transform.position = new Vector3(pos.X * 10f, pos.Y * 10f);
                }
                
            }
        }
	}
}
